import java.util.Scanner;
public class Mittelwert {

	public static void main(String[] args) {
		programmHinweis();
		double zahl1 = 0.0;
		double zahl2 = 0.0;
		double zahl3 = 0.0;
		double ergebnis2, ergebnis3;
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben sie die erste Zahl ein:");
		zahl1 = myScanner.nextDouble();
		System.out.println("Geben sie die zweite Zahl ein:");
		zahl2 = myScanner.nextDouble();
		System.out.println("Geben sie die dritte Zahl ein:");
		zahl3 = myScanner.nextDouble();
		
		ergebnis2 = mittelwertBerechnung(zahl1, zahl2);
		ergebnis3 = mittelwertBerechnung(zahl1, zahl2, zahl3);
		
		
		
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", zahl1, zahl2, ergebnis2);
		System.out.printf("Der Mittelwert von %.2f, %.2f und %.2f ist %.2f\n", zahl1, zahl2, zahl3, ergebnis3);
		
		myScanner.close();
 // R�ckgabendatentyp - Methodenname - Parameterliste
	}
	
	static double mittelwertBerechnung(double zahlEins, double zahlZwei) { //Signatur der Methode
		
		double ergebnisMethode = 0.0;
		
		ergebnisMethode = (zahlEins + zahlZwei) / 2.0;  //Methodenrumpf
		
		return ergebnisMethode;
	}
	
	static double mittelwertBerechnung(double zahlEins, double zahlZwei, double zahlDrei) { //Signatur der Methode
		
		double ergebnisMethode = 0.0;
		
		ergebnisMethode = (zahlEins + zahlZwei + zahlDrei) / 2.0;  //Methodenrumpf
		
		return ergebnisMethode;
	}
	
	static void programmHinweis() {
		System.out.println("Dieses Programm errechnet den Mittelwert usw");
	}
}
