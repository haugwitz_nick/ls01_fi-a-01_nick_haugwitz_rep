﻿import java.util.Scanner;

class Fahrkartenautomat
{
    static double zuZahlenderBetrag, eingezahlterGesamtbetrag, eingeworfeneMünze, rückgabebetrag;
    static int anzahltickets, Land1 = 10, Land2 = 5, Land3 = 15, Land4 = 8;
    static Scanner tastatur = new Scanner(System.in);

    public static void main(String[] args)
    {
        //Main-Methode
        //Schleife dient zum Neustart, nachdem das Programm beendet wurde
        while (true) {
            zuZahlenderBetrag = anzahlTickets();
            eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
            fahrkartenAusgeben();
            System.out.println("\n\n");
            rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
            rueckgeldAusgabe();
        }
    }

    //FahrkartenbestellungErfassen
    public static double anzahlTickets() {
        int Landwahl;
        boolean isCorrect = false;

        //Ausgabe der möglichen reiseziele
        System.out.println("Guten Tag, wohin wollen Sie fahren?");
        System.out.println("-----------------------------------");
        System.out.printf("%-20s %s", "1| Japan", "10 Euro\n");
        System.out.printf("%-20s %s", "2| Frankreich", "5 Euro\n");
        System.out.printf("%-20s %s", "3| USA", "15 Euro\n");
        System.out.printf("%-20s %s", "4| Russland", "8 Euro\n");
        System.out.println("-----------------------------------");

        //Abfrage welches Land gewählt wurde mit Falscheingabenüberprüfung
        while (!isCorrect) {
            Landwahl = tastatur.nextInt();
            if (Landwahl == 1) {
                zuZahlenderBetrag = Land1;
                isCorrect = true;
            } else if (Landwahl == 2) {
                zuZahlenderBetrag = Land2;
                isCorrect = true;
            } else if (Landwahl == 3) {
                zuZahlenderBetrag = Land3;
                isCorrect = true;
            } else if (Landwahl == 4) {
                zuZahlenderBetrag = Land3;
                isCorrect = true;
            } else {
                System.out.println("Das ist keine gültige Eingabe!");
                System.out.println("Bitte nehmen Sie die Eingabe erneut vor!");
            }
        }
        isCorrect = false;
        //Agfrage der zu kaufenden Tickets mit Falscheingabenüberprüfung
        System.out.println("Wie viele Tickets möchten Sie kaufen?");
        while (!isCorrect){
            anzahltickets = tastatur.nextInt();
            if(anzahltickets <= 10){
                isCorrect = true;
            }else{
                System.out.println("Das ist keine gültige Eingabe!");
                System.out.println("Es können maximal 10 Tickets angefordert werden!");
                System.out.println("Bitte nehmen Sie die Eingabe erneut vor!");
            }
        }

        zuZahlenderBetrag = zuZahlenderBetrag * anzahltickets;

        return zuZahlenderBetrag;
    }

    //Fahrkarten bezahlen
    public static double fahrkartenBezahlen(double zuZahlen) {
        eingezahlterGesamtbetrag = 0.0;
        boolean isCorrect = false;
    //Berechnen was noch zu zahlen ist
        while(eingezahlterGesamtbetrag < zuZahlen){
            System.out.printf("Noch zu zahlen: %.2f €\n" , (zuZahlen - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            isCorrect = false;
            while (!isCorrect) {
                eingeworfeneMünze = tastatur.nextDouble();
                if (eingeworfeneMünze == 0.05 || eingeworfeneMünze == 0.10 || eingeworfeneMünze == 0.20 || eingeworfeneMünze == 0.50 || eingeworfeneMünze == 1 || eingeworfeneMünze == 2){
                    isCorrect = true;
                }else{
                    System.out.println("Kein gültiges Zahlungsmittel!");
                }
            }
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag;
    }

    //Fahrkartenausgabe
    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //Rückgeldausgabe
    public static void rueckgeldAusgabe() {
        //Ausgabe von passenden münzen mit Schleifen
        if (rückgabebetrag > 0.0) {
                System.out.printf("Der Rückgabebetrag in Höhe von %.2f €\n", rückgabebetrag);
                System.out.println("wird in folgenden Münzen ausgezahlt:");
                while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
                {
                    System.out.println("2 EURO");
                    rückgabebetrag -= 2.0;
                }
                while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
                {
                    System.out.println("1 EURO");
                    rückgabebetrag -= 1.0;
                }
                while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
                {
                    System.out.println("50 CENT");
                    rückgabebetrag -= 0.5;
                }
                while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
                {
                    System.out.println("20 CENT");
                    rückgabebetrag -= 0.2;
                }
                while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
                {
                    System.out.println("10 CENT");
                    rückgabebetrag -= 0.1;
                }
                while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
                {
                    System.out.println("5 CENT");
                    rückgabebetrag -= 0.05;
                }
            }
            System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                    "vor Fahrtantritt entwerten zu lassen!\n" +
                    "Wir wünschen Ihnen eine gute Fahrt.");

        //Runterscrollen damit die Abfrage (Das Programm) von vorne beginnt
        for (int i = 0; i < 8; i++) {
            System.out.println("\n");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        }
    }
