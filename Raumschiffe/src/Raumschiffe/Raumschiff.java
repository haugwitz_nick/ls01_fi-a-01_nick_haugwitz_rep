package Raumschiffe;
import java.util.ArrayList;


public class Raumschiff {

	// Variablen
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	// Konstruktor
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}

	// Getter und Setter
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl2) {
		photonentorpedoAnzahl = photonentorpedoAnzahl2;
	}

	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent2) {
		energieversorgungInProzent = energieversorgungInProzent2;
	}

	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent2) {
		schildeInProzent = schildeInProzent2;
	}

	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent2) {
		huelleInProzent = huelleInProzent2;
	}

	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}

	public void setLebenserhaltungssystemInProzent(int lebenserhaltungssystemInProzent2) {
		lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent2;
	}

	public int getLebenserhaltungssystemInProzent() {
		return this.lebenserhaltungssystemInProzent;
	}

	public void setAndroidenAnzahl(int androidenAnzahl2) {
		androidenAnzahl = androidenAnzahl2;
	}

	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}

	public void setSchiffsname(String schiffsname2) {
		schiffsname = schiffsname2;
	}

	public String getSchiffsname() {
		return this.schiffsname;
	}

	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}

	public void statusRaumschiff() {
		System.out.println("Zustand des Raumschiffes " + this.getSchiffsname() + " :" + "\nAnzahl der Photonentopedos: "
				+ this.getPhotonentorpedoAnzahl() + "\nEnergiversorgung des Raumschiffes: "
				+ this.getEnergieversorgungInProzent() + "\nZustand der Schilde: " + this.getSchildeInProzent()
				+ "\nZustand der H�lle: " + this.getHuelleInProzent() + "\nZustand der Lebenserhaltungssysteme: "
				+ this.getLebenserhaltungssystemInProzent() + "\nAnzahl der Androiden: " + this.getAndroidenAnzahl());
	}

	public void ladungsverzeichnisAusgaben() {
		System.out.println("Zustand des Raumschiffes " + this.getSchiffsname() + "enth�lt folgende Ladungen:");
		for (Ladung tmp : ladungsverzeichnis) {
			System.out.println(tmp.getBezeichnung() + " " + tmp.getMenge());
		}
	}

	public void PhotonentorpedoSchiessen(Raumschiff h) {
		if (getPhotonentorpedoAnzahl() <= 0) {
			System.out.println("=*Click*=\n");
		} else {
			setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() - 1);
			System.out.println("Photonentorpedos wurden abgeschossen!\n");
			hit(h);
			treffer(h);

		}
	}
	
	public void PhaserKanoneSchiessen (Raumschiff h) {
		if (getEnergieversorgungInProzent() < 50 ) {
		  System.out.println("=*Click*=");
		}else {
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() - 50);
			System.out.println("Phaserkanone abgeschossen!");
			hit(h);
			treffer(h);
		}
	}

	private void hit(Raumschiff h) {
		System.out.println(this.getSchiffsname() + "wurde getroffen!");
	}
	
	public void nachrichtanalle(String message) {
		broadcastKommunikator.add(message);
	}
	
	public static ArrayList<String> Logbucheintraegerueckgabe(){
		return broadcastKommunikator;		
	}
	
	private void treffer(Raumschiff h) {
		h.setSchildeInProzent(h.getSchildeInProzent()-50);
		if (h.getSchildeInProzent() <= 0) {
			h.setHuelleInProzent(h.getHuelleInProzent()-50);
			h.setEnergieversorgungInProzent(h.getEnergieversorgungInProzent()-50);
			if (h.getHuelleInProzent() <= 0) {
				h.setLebenserhaltungssystemInProzent(0);
				nachrichtanalle("Die Lebenserhaltungssysteme wurden nun vernichtet!");
			}
		}
		
	}

}
