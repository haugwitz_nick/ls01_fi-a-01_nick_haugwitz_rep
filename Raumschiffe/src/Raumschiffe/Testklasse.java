package Raumschiffe;

public class Testklasse {

	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff(8, 100, 100, 100, 100, 16, "IKS Hegh`ta");
		Raumschiff romulaner = new Raumschiff(8, 100, 100, 100, 100, 16, "IKS Khazara");
		Raumschiff vulkanier = new Raumschiff(8, 100, 100, 100, 100, 16, "Ni'Var");
		
		Ladung ladungkl1 = new Ladung("Ferrengi Schneckensaft", 200);
		Ladung ladungkl2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		
		klingonen.addLadung(ladungkl1);
		klingonen.addLadung(ladungkl2);
		
		Ladung ladungro1 = new Ladung("Borg-Schrott", 5);
		Ladung ladungro2 = new Ladung("Rote Materie", 2);
		Ladung ladungro3 = new Ladung("Plasma-Waffe", 50);
		
		romulaner.addLadung(ladungro1);
		romulaner.addLadung(ladungro2);
		romulaner.addLadung(ladungro3);
		
		Ladung ladungvu1 = new Ladung("Forschungssonde", 35);
		Ladung ladungvu2 = new Ladung("Photonentorpedo", 3);
		
		vulkanier.addLadung(ladungvu1);
		vulkanier.addLadung(ladungvu2);
		
		klingonen.PhotonentorpedoSchiessen(romulaner);
		romulaner.PhaserKanoneSchiessen(klingonen);
		vulkanier.nachrichtanalle("Gewalt ist nicht logisch");
		klingonen.statusRaumschiff();
		klingonen.ladungsverzeichnisAusgaben();
		klingonen.PhotonentorpedoSchiessen(romulaner);
		klingonen.PhotonentorpedoSchiessen(romulaner);
		klingonen.statusRaumschiff();
		romulaner.statusRaumschiff();
		vulkanier.statusRaumschiff();
		klingonen.ladungsverzeichnisAusgaben();
		romulaner.ladungsverzeichnisAusgaben();
		vulkanier.ladungsverzeichnisAusgaben();
		System.out.println(Raumschiff.Logbucheintraegerueckgabe());
	}

}
