package Raumschiffe;

public class Ladung {
	
	//Variablen
	private String Bezeichnung;
	private int Menge;
	
	//Konstruktor
	public Ladung(String bezeichnung, int menge) {
		this.Bezeichnung = bezeichnung;
		this.Menge = menge;
	}
	
	//Getter und Setter
	 public void setBezeichnung(String Bezeichnung2) {
		 Bezeichnung = Bezeichnung2;
		 }
		 
		 public String getBezeichnung() {
		    return this.Bezeichnung;
		 }
		 
	 public void setMenge(int Menge2) {
		 Menge = Menge2;
		 }
			 
		 public int getMenge() {
			 return this.Menge;
		 }

	

}
