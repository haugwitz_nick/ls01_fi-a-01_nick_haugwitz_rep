import java.util.Scanner;
public class Methodenuebung {

    public static void main(String[] args) {
        double zahl1 =  0.0,  zahl2 = 0.0,   erg = 0.0;
        programmhinweis();
        zahl1 = eingabe("1. Zahl: ");
        zahl2 = eingabe("2. Zahl: ");
        erg = berechnung(zahl1, zahl2);
        ausgabe( zahl1, zahl2, erg);
        
    }
        public static void programmhinweis() {
            System.out.println("Hinweis");
            System.out.println("Das Programm multipliziert 2 eingegebene Zahlen.");
            
        }
        public static void ausgabe(double zahl1, double zahl2, double erg) {
            System.out.println("Ergebnis der Multiplikation: ");
            System.out.printf("%.2f * %.2f = %.2f%n", zahl1, zahl2, erg);    
        }
        
        public static double berechnung(double zahl1, double zahl2) {
        double ergebnis;
        ergebnis = zahl1 * zahl2;
        return ergebnis;
    }

        public static double eingabe(String text) {
            Scanner sc = new Scanner (System.in);
            System.out.print(text);
            double zahl = sc.nextDouble();
            return zahl;
        }
}